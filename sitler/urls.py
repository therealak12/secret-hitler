from django.contrib import admin
from django.urls import include, path

api_urlpatterns = [
    path('users/', include('users.urls')),
    path('game/', include('game.urls')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(api_urlpatterns)),
]
