from django.http import JsonResponse
from rest_framework.decorators import permission_classes, authentication_classes, api_view
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from game.models import Game


@api_view(http_method_names=['POST'])
@permission_classes((IsAuthenticated,))
@authentication_classes((TokenAuthentication,))
def create_game(request):
    player_count = request.data['player_count']
    game = Game.objects.create(player_count=player_count)
    return JsonResponse({'join_url': game.join_url + '/'})


@api_view(http_method_names=['GET'])
@permission_classes((IsAuthenticated,))
@authentication_classes((TokenAuthentication,))
def list_games(request):
    games_info = []
    for game in Game.objects.filter(state=Game.WAITING):
        games_info.append({'join_url': game.join_url + '/', 'players_to_start': game.player_count - game.joined_player_count})
    return JsonResponse(games_info, safe=False)
