import secrets

from django.db import models
from django.contrib.auth import get_user_model

INIT_FASCIST_CARDS = 11
INIT_LIBERAL_CARDS = 6


def generate_join_url(length=5):
    join_url = secrets.token_urlsafe(length)
    while Game.objects.filter(join_url=join_url):
        join_url = secrets.token_urlsafe(length)
    return join_url


class Game(models.Model):
    FINISHED = 'F'
    ACTIVE = 'A'
    WAITING = 'W'
    GAME_STATES = [
        (FINISHED, 'Finished'),
        (ACTIVE, 'Active'),
        (WAITING, 'Waiting'),
    ]
    state = models.CharField(max_length=1, choices=GAME_STATES, default=WAITING)
    join_url = models.CharField(max_length=8, unique=True, db_index=True, default=generate_join_url)
    player_count = models.PositiveSmallIntegerField()
    joined_player_count = models.PositiveSmallIntegerField(default=0)
    current_president = models.OneToOneField(
        get_user_model(), on_delete=models.PROTECT, related_name='president_game', blank=True, null=True)
    current_chancellor = models.OneToOneField(
        get_user_model(), on_delete=models.PROTECT, related_name='chancellor_game', blank=True, null=True)
    hitler = models.OneToOneField(
        get_user_model(), on_delete=models.PROTECT, related_name='hitler_game', blank=True, null=True)
    remaining_fascist_count = models.PositiveSmallIntegerField(
        default=INIT_FASCIST_CARDS)
    remaining_liberal_count = models.PositiveSmallIntegerField(
        default=INIT_LIBERAL_CARDS)

    def new_player_joined(self):
        self.joined_player_count += 1
        self.save()

    def can_accept_player(self):
        return self.joined_player_count < self.player_count

    def is_ready_to_start(self):
        return self.joined_player_count == self.player_count

    def select_roles(self):
        users = self.user_set.all()
        for i, user in enumerate(users):
            if i == 0:
                user.current_role = 'h'
                user.save()
            elif i == 1:
                user.current_role = 'f'
                user.save()
            else:
                user.current_role = 'l'
                user.save()
                print(user.current_role)
