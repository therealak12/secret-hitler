import json
import random
import sys

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from rest_framework.authtoken.models import Token

from game.models import Game


class GameConsumer(WebsocketConsumer):
    def connect(self):
        self.join_url = self.scope['url_route']['kwargs']['join_url']
        self.group_name = 'chat_%s' % self.join_url
        try:
            self.game = Game.objects.get(join_url=self.join_url)
        except Game.DoesNotExist:
            self._refuse_connection_with_error('Invalid join url')
            return
        if not self.game.can_accept_player():
            self._refuse_connection_with_error('Game capacity is full')

        self.game.new_player_joined()
        async_to_sync(self.channel_layer.group_add)(
            self.group_name,
            self.channel_name
        )
        self.accept()
        if self.game.is_ready_to_start():
            self._send_start_game_message()

    def disconnect(self, close_code):
        print('disconnecting')
        if hasattr(self, 'game'):
            self.game.joined_player_count -= 1
            self.game.save()
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name,
            self.channel_name
        )

    def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        header = text_data_json['header']
        if header == 'token':
            self._check_user(text_data_json)
            print(self.game.user_set.count())
            print(self.game.player_count)
            if self.game.user_set.count() == self.game.player_count:
                self._send_players()
                self.game.select_roles()
                async_to_sync(self.channel_layer.group_send)(
                    self.group_name,
                    {
                        'type': 'role_select',
                    }
                )
                self.game.current_president = self.game.user_set.all()[
                    random.randint(0, self.game.user_set.count() - 1)]
                self.game.save()
                async_to_sync(self.channel_layer.group_send)(
                    self.group_name,
                    {
                        'type': 'send_president',
                        'username': self.game.current_president.username
                    }
                )

    def send_president(self, event):
        self.send(text_data=json.dumps({
            'header': 'president',
            'username': event['username']
        }))

    def game_start(self, event):
        self.send(text_data=json.dumps({
            'header': 'game_start',
        }))

    def _check_user(self, json_data):
        if 'user' in self.scope:
            return
        try:
            token = json_data['token']
            token = Token.objects.get(key=token)
            self.scope['user'] = token.user
            token.user.game = self.game
            token.user.save()
        except Token.DoesNotExist:
            self._refuse_connection_with_error('')

    def role_select(self, event):
        data = {
            'header': 'role',
            'role': self.scope['user'].current_role
        }
        if data['role'] == 'h':
            for user in self.game.user_set.all():
                if user.current_role == 'f':
                    data.update({'yar': user.username})
        elif data['role'] == 'f':
            for user in self.game.user_set.all():
                if user.current_role == 'h':
                    data.update({'yar': user.username})
        self.send(text_data=json.dumps(data))

    def _refuse_connection_with_error(self, error_message):
        self.accept()
        self.send(text_data=json.dumps({'header': 'error', 'error': error_message}))
        self.close()

    def _send_start_game_message(self):
        async_to_sync(self.channel_layer.group_send)(
            self.group_name,
            {
                'type': 'game_start'
            }
        )

    def send_players(self, event):
        self.send(text_data=json.dumps({
            'header': 'players',
            'players': json.dumps([{'username': user.username} for user in self.game.user_set.all()])
        }))

    def _send_players(self):
        async_to_sync(self.channel_layer.group_send)(
            self.group_name,
            {
                'type': 'send_players'
            }
        )
