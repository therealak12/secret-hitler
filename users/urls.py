from django.urls import path, include
from rest_framework.authtoken import views as authtoken_views

from users import views

app_name = 'users'

urlpatterns = [
    path('obtain-token/', authtoken_views.obtain_auth_token),
    path('create/', views.CreateUserView.as_view()),
]