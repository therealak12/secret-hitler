from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator

from game import routing as game_routing

application = ProtocolTypeRouter({
    'websocket': URLRouter(
        game_routing.websocket_urlpatterns
    )
})
