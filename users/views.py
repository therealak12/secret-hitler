from users.serializers import UserSerializer
from rest_framework.generics import CreateAPIView

from users.serializers import UserSerializer


class CreateUserView(CreateAPIView):
    serializer_class = UserSerializer
