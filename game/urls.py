from django.urls import path

from game.views import create_game, list_games

app_name = 'game'

urlpatterns = [
    path('create/', create_game, name='game-create'),
    path('list/', list_games, name='game-list'),
]
